package com.example.landingpage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    public static final String EXTRA_MESSAGE =
            "com.example.android.twoactivities.extra.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(LOG_TAG, "------");
        Log.d(LOG_TAG, "onCreate");
    }


    public void launchLogin(View view) {
        Log.d(LOG_TAG, "Button clicked!");
        Intent intent = new Intent(this, register.class);
        startActivity(intent);
    }

    public void launchRegister(View view) {
        Log.d(LOG_TAG, "Button clicked!");
        Intent intent = new Intent(this, login.class);
        startActivity(intent);
    }
}

